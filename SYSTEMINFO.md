# phpIPAM

Vendor: phpIPAM – Open source IP address management
Homepage: https://phpipam.net/

Product: phpIPAM
Product Page: https://phpipam.net/

## Introduction

We classify phpIPAM into the Inventory domain as phpIPAM's core functionality and features are related IP address management (IPAM), asset tracking and resource inventory. We also classify it into the Network Services domain due to its role in facilitating various network-related services and functionalities.

"Its goal is to provide light, modern and useful IP address management"

## Why Integrate
The phpIPAM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with phpIPAM .

With this adapter you have the ability to perform operations with phpIPAM such as:

- IPAM
- VLANs
- VRFs
- Layer 2 domains

## Additional Product Documentation
The [API documents for phpIPAM](https://phpIPAM.dev.ns.internet2.edu/api/docs/)