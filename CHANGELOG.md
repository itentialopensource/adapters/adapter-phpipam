
## 2.3.4 [10-15-2024]

* Changes made at 2024.10.14_21:26PM

See merge request itentialopensource/adapters/adapter-phpipam!21

---

## 2.3.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-phpipam!19

---

## 2.3.2 [08-14-2024]

* Changes made at 2024.08.14_19:44PM

See merge request itentialopensource/adapters/adapter-phpipam!18

---

## 2.3.1 [08-07-2024]

* Changes made at 2024.08.06_21:49PM

See merge request itentialopensource/adapters/adapter-phpipam!17

---

## 2.3.0 [05-10-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/inventory/adapter-phpipam!16

---

## 2.2.5 [03-26-2024]

* Changes made at 2024.03.26_14:48PM

See merge request itentialopensource/adapters/inventory/adapter-phpipam!15

---

## 2.2.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-phpipam!14

---

## 2.2.3 [03-13-2024]

* Changes made at 2024.03.13_14:05PM

See merge request itentialopensource/adapters/inventory/adapter-phpipam!13

---

## 2.2.2 [03-11-2024]

* Changes made at 2024.03.11_14:14PM

See merge request itentialopensource/adapters/inventory/adapter-phpipam!12

---

## 2.2.1 [02-26-2024]

* Changes made at 2024.02.26_13:43PM

See merge request itentialopensource/adapters/inventory/adapter-phpipam!11

---

## 2.2.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-phpipam!10

---

## 2.1.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-phpipam!9

---

## 2.0.4 [03-11-2021]

- Migration to bring up to the latest foundation
	- Change to .eslintignore (adapter_modification directory)
	- Change to README.md (new properties, new scripts, new processes)
	- Changes to adapterBase.js (new methods)
	- Changes to package.json (new scripts, dependencies)
	- Changes to propertiesSchema.json (new properties and changes to existing)
	- Changes to the Unit test
	- Adding several test files, utils files and .generic entity
	- Fix order of scripts and dependencies in package.json
	- Fix order of properties in propertiesSchema.json
	- Update sampleProperties, unit and integration tests to have all new properties.
	- Add all new calls to adapter.js and pronghorn.json
	- Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-phpipam!8

---

## 2.0.3 [08-24-2020]

- Update address did not seem to accept a passed in id which is required in the path. This has been added now

See merge request itentialopensource/adapters/inventory/adapter-phpipam!7

---

## 2.0.2 [07-28-2020]

- Added in path variables that are required for the call to work properly

See merge request itentialopensource/adapters/inventory/adapter-phpipam!6

---

## 2.0.1 [07-09-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-phpipam!5

---

## 1.0.0 [07-09-2020]

- Add coverage for all API calls and global parameters. Tested during a POC

See merge request itentialopensource/adapters/inventory/adapter-phpipam!4

---

## 1.2.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-phpipam!3

---

## 1.2.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/inventory/adapter-phpipam!2

---

## 1.1.0 [09-19-2019]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-phpipam!1

---
## 1.0.3 [07-31-2019] & 1.0.2 [07-30-2019] & 1.0.1 [07-19-2019]

- Initial Commit

See commit 7fd76b8

---
